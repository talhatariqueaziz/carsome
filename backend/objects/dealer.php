<?php
	//dealer object
	class Dealer{
		//database connection and table name
		private $conn;
		private $table_name = "dealer";

		//object properties
		public $id, $name;

		//constructor
		public function __construct($db){
			$this->conn = $db;
		}

		
		//get dealer
		function getDealer(){
			//query
			$query = "SELECT * FROM " . $this->table_name . " WHERE id = :id LIMIT 0,1";

			//prepare query
			$stmt = $this->conn->prepare($query);

			//bind param
			$stmt->bindParam(":id", $this->id);

			//return query
			if($stmt->execute()){
				//get object record
				$row = $stmt->fetch(PDO::FETCH_ASSOC);

				//assign object variable with fetch data
				$this->id = $row["id"];
				$this->name = $row["name"];
		
				//return true upon success
				return true;
			}
			return false; //return false upon failure
		}
		
		//get all dealers
		function getAllDealer(){
			
			//query
			$query = "SELECT * FROM " . $this->table_name;

			//prepare query
			$stmt = $this->conn->prepare($query);

			//execute query
			$stmt->execute();

			//return result
			return $stmt;
		}

		//sanitizer id
		function sanitizer(){
			$this->name = htmlspecialchars(strip_tags($this->name));
		}
	}
?>
