<?php
	//bid object
	class Bid{
		//database connection and table nam
		private $conn;
		private $table_name = "bid";

		//object properties
		public $id, $dealer_id, $proposed_bid, $bid_status, $creation_date;

		//constructor
		public function __construct($db){
			$this->conn = $db;
		}

		//create bid
		function createBid(){
			//query
			$query = "INSERT INTO " . $this->table_name . "(dealer_id, proposed_bid, bid_status) VALUES(:dealer_id, :proposed_bid, :bid_status)";

			//prepare query
			$stmt = $this->conn->prepare($query);

			//sanitizer
			$this->sanitizer();

			//bind query param
			$stmt->bindParam(":dealer_id", $this->dealer_id);
			$stmt->bindParam(":proposed_bid", $this->proposed_bid);
			$stmt->bindParam(":bid_status", $this->bid_status);

			//execute query
			if($stmt->execute())
				return true; //return true upon success
			else
				return false; //return false upon failure
		}

		//get bid
		function getBid(){
			//qeury
			$query = "SELECT * FROM " . $this->table_name . " WHERE id = :id LIMIT 0,1";

			//prepare query
			$stmt = $this->conn->prepare($query);

			//bind query param
			$stmt->bindParam(":id", $this->id);

			//return query
			if($stmt->execute()){
				//get object record
				$row = $stmt->fetch(PDO::FETCH_ASSOC);

				//assign object variable with fetch data
				$this->id = $row["id"];
				$this->dealer_id = $row["dealer_id"];
				$this->proposed_bid = $row["proposed_bid"];
				$this->bid_status = $row["bid_status"];
				$this->creation_date = $row["creation_date"];

				//return true upon success
				return true;
			}
			return false; //return false upon failure
		}

		//get all bid
		function getAllBid(){
			//query
			$query = "SELECT * FROM " . $this->table_name;

			//prepare query
			$stmt = $this->conn->prepare($query);

			//execute query
			$stmt->execute();

			//return result
			return $stmt;
		}

		//get dealer total bids
		function getDealerBidExceed(){
			//query
			$query = "SELECT COUNT(id) AS totalBids FROM " . $this->table_name . " WHERE dealer_id = :dealer_id";

			//prepare query
			$stmt = $this->conn->prepare($query);
			
			//bind query param
			$stmt->bindParam(":dealer_id", $this->dealer_id);

			//execute query
			if($stmt->execute()){
				//get object record
				$row = $stmt->fetch(PDO::FETCH_ASSOC);	
				if($row["totalBids"] < 20){
					return true;
				}
			}
			return false;
		}

		//get dealer all approved bid
		function getDealerBidsApproved(){
			//query
			$query = "SELECT COUNT(id) AS totalApprovedBids FROM " . $this->table_name . 
				" WHERE dealer_id = :dealer_id AND bid_status = true";

			//prepare query
			$stmt = $this->conn->prepare($query);

			//bind query param
			$stmt->bindParam(":dealer_id", $this->dealer_id);

			//execute query
			if($stmt->execute()){
				//get object record
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				if($row["totalApprovedBids"] == 0)
					return 0;
				return $row["totalApprovedBids"];
			}
		}
			
		//get dealer all rejected bid
		function getDealerBidsRejected(){
			//query
			$query = "SELECT COUNT(id) AS totalRejectedBids FROM " . $this->table_name . 
				" WHERE dealer_id = :dealer_id AND bid_status = false";

			//prepare query
			$stmt = $this->conn->prepare($query);

			//bind query param
			$stmt->bindParam(":dealer_id", $this->dealer_id);

			//execute query
			$stmt->execute();

			if($stmt->execute()){
				//get object record
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				if($row["totalRejectedBids"] == 0)
					return 0;
				return $row["totalRejectedBids"];
			}
		}

		
		//get dealer all approved bid sum
		function getDealerBidsApprovedSum(){
			//query
			$query = "SELECT SUM(proposed_bid) AS totalApprovedSum FROM " . $this->table_name . 
				" WHERE dealer_id = :dealer_id AND bid_status = true";

			//prepare query
			$stmt = $this->conn->prepare($query);

			//bind query param
			$stmt->bindParam(":dealer_id", $this->dealer_id);

			//execute query
			if($stmt->execute()){
				//get object record
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				if($row["totalApprovedSum"] == 0)
					return 0;
				return $row["totalApprovedSum"];
			}

		}

		//get bid per minutes
		function getBidPerSeconds(){
			$totalBids = 0;
			$firstBidTime;
			$lastBidTime;
			$totalTimeMinutes;
			$totalBidsPerSeconds = 0;

			//query
			$query = "SELECT COUNT(id) AS totalBids FROM " . $this->table_name;

			//prepare query
			$stmt = $this->conn->prepare($query);

			//execute query
			if($stmt->execute()){
				//get object record
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				if($row["totalBids"] > 0){
					$this->totalBids = $row["totalBids"];

					//query firstBidTime
					$query1 = "SELECT creation_date as creationDate FROM " . $this->table_name . " ORDER BY ID ASC LIMIT 1";
					//prepare query
					$stmt1 = $this->conn->prepare($query1);
					//execute query
					$stmt1->execute();
					//get object record
					$row1 = $stmt1->fetch(PDO::FETCH_ASSOC);
					$this->firstBidTime = date($row1["creationDate"]);
					
					//query firstBidTime
					$query2 = "SELECT creation_date as creationDate FROM " . $this->table_name . " ORDER BY ID DESC LIMIT 1";
					//prepare query
					$stmt2 = $this->conn->prepare($query2);
					//execute query
					$stmt2->execute();
					//get object record
					$row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
					$this->lastBidTime = date($row2["creationDate"]);

					//difference
					$this->totalTimeMinutes = round(abs(strtotime($this->lastBidTime) - strtotime($this->firstBidTime)) /60,3);
					
					//total bids per seconds
					$this->totalBidsPerSeconds = $this->totalBids / ($this->totalTimeMinutes*60);

					//return result
					return $this->totalBidsPerSeconds;
				}
			}
			
		}

		//sanitizer user input
		function sanitizer(){
			$this->proposed_bid = htmlspecialchars(strip_tags($this->proposed_bid));
		}
	}
?>
