<?php
	//include header
	include_once "../../include/header.php";

	//file require for database connection and objects
	include_once "../../include/requiredFiles.php";

	//get database connection
	$database = new Database();
	$db = $database->getConnection();

	//initialize object
	$dealer = new Dealer($db);
	$bid = new Bid($db);

	//get url parameters
	$request = $_REQUEST["request"];

	switch($request){
		case "getDealer": //get specefic dealer data
			$dealer->id = $_REQUEST["id"];
			if($dealer->getDealer()){
				//set response code & message
				http_response_code(200);
				echo json_encode(array("data" => $dealer));
			}else{
				//set response code & message
				http_response_code(204);
				echo json_encode(array("message" => "failed to get dealer."));
			}
			break;

		case "getAllDealer": //get all dealers data
			$stmt = $dealer->getAllDealer();
			if($stmt->rowCount() > 0){
				//initializing array
				$dealer_array["data"] = array();
			
				//retrieve content
				while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
					//extract row
					extract($row);
					//map object variable to feteched data
					$dealer = array(
						"id" => $id,
						"name" => $name
					);
	
					//add object to array
					array_push($dealer_array["data"], $dealer);
				}	
				//set response code & message
				http_response_code(200);
				echo json_encode($dealer_array);
			}else{
				//set response code & message
				http_response_code(204);
				echo json_encode(array("message" => "failed to get all dealers."));
			}
			break;


		case "getBid": //get specefic bid data
			$bid->id = $_REQUEST["id"];
			if($bid->getBid()){
				//set reponse code & message
				http_response_code(200);
				echo json_encode(array("data" => $bid));
			}else{
				//set response code & message
				http_response_code(204); 
				echo json_encode(array("message" => "failed to get bid."));
			}
			break;

		case "getAllBid": //get all bids data
			$stmt = $bid->getAllBid();
			if($stmt->rowCount() > 0){
				//initialzing array 
				$bid_array["data"] = array(); 
				
				//retrieve content
				while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
					//extract row
					extract($row);

					//map object variable to feteched data
					$bid = array(
						"id" => $id,
						"dealer_id" => $dealer_id,
						"proposed_bid" => $proposed_bid,
						"bid_status" => $bid_status,
						"creation_date" => $creation_date
					);

					//add object to array
					array_push($bid_array["data"], $bid);

					//set response code & message
					http_response_code(200);
					echo json_encode($bid_array);
				}
			}else{
				//set response code & message
				http_response_code(204);
				echo json_encode(array("message" => "failed to get all bids."));
			}
			break;
		
		case "getAllDealersBids": //get all dealers bid data
			$stmt = $dealer->getAllDealer();
			if($stmt->rowCount() > 0){
				//initializing array
				$dealer_array["data"] = array();
			
				//retrieve content
				while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
					//extract row
					extract($row);
							
					//fetching dealer bid data
					$bid->dealer_id = $row["id"];
					
					$total_bids_approved = $bid->getDealerBidsApproved();
					$total_bids_rejected = $bid->getDealerBidsRejected();
					$total_bids_approved_sum = $bid->getDealerBidsApprovedSum();

					//map object variable to fetched data
					$dealer = array(
						"id" => $id,
						"name" => $name,
						"total_bids_approved" => $total_bids_approved,
						"total_bids_rejected" => $total_bids_rejected,
						"total_bids_approved_sum" => $total_bids_approved_sum
					);

					//add object to array
					array_push($dealer_array["data"], $dealer);
				}
				//set response code & message
				http_response_code(200);
				echo json_encode($dealer_array);
			}else{
				//set response code & message
				http_response_code(204);
				echo json_encode(array("message" => "failed to get all dealer bids."));
			}
			break;

		case "getBPS":
			$bps = $bid->getBidPerSeconds();

			//set response code & message
			http_response_code(200);
			echo json_encode(array("data" => $bps));
			break;
	}


?>
