<?php
        //include header
        include_once "../../include/header.php";

        //file require for database connection and objects
        include_once "../../include/requiredFiles.php";

        //get database connection
        $database = new Database();
        $db = $database->getConnection();

        //initialize object
        $dealer = new Dealer($db);
        $bid = new Bid($db);

        //get url parameters
        $request = $_REQUEST["request"];


	switch($request){
		case "createBid":
			$bid->dealer_id = $_REQUEST["dealer_id"];
			$bid->proposed_bid = $_REQUEST["proposed_bid"];
			$bid->bid_status = $bid->getDealerBidExceed();	

			if($bid->createBid() && $bid->bid_status){
				//set response code & messagei
				http_response_code(201);
				echo json_encode(array("message" => "bid has been created successfully without exceed limit."));
			}elseif ($bid->createBid() && !$bid->bid_status){
				//set response code & message
				http_response_code(207);
				echo json_encode(array("message" => "bid has been created successfully with exceed limit."));
			}else{
				//set response code & message
				http_response_code(207);
				echo json_encode(array("message" => "failed to create bid."));
			}
			break;
	}	
?>
