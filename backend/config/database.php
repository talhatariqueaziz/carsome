<?php
	//object database
	class Database{
		//variable
		private $host = "localhost";
		private $db = "carsome";
		private $username = "newuser";
		private $pass = "newPass#123";
		private $conn;

		//establish db connection
		function getConnection(){
			$this->conn = null;

			try{
				$this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db, $this->username, $this->pass);
			}catch(PDOEXCEPTION $exception){
				echo "Connection error: " . $exception->getMessage();
			}
			return $this->conn;
		}
	}
?>
