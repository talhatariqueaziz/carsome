<?php
	//database connection
	$host = "localhost";
	$username = "root";
	$pass = "newPass#123";

	//establish connection
	$conn = new mysqli($host, $username, $pass);

	//check connection
	if($conn->connect_error)
		die("Connection failed: " . $conn->connect_error);



	//dropping database if exists
	$sql_drop = "DROP DATABASE IF EXISTS carsome";
	//execute query drop database
	if($conn->query($sql_drop)){
		echo "Database dropped successfully.";
		$conn->next_result();
	}else
		echo "Error dropping database: " . $conn->error;



	//creating a new root user
        $sql_user = "CREATE USER IF NOT EXISTS 'newuser'@'localhost' IDENTIFIED BY 'newPass#123';";
        $sql_user .= "GRANT ALL ON *.* TO 'newuser'@'localhost';";
        $sql_user .= "FLUSH PRIVILEGES;";
        //execute qeury creating user
        if(!$conn->multi_query($sql_user))
                echo "\r\nError creating new user: " . $conn->error;
        do{
                if($result = $conn->store_result()){
                        $result->free();
                }
                if(!$conn->next_result())
                        echo "\r\nNew root user has been created successfully.";
	}while($conn->next_result());


	
	//creating database
	$sql_db = "CREATE DATABASE IF NOT EXISTS carsome";
	//execute query create database
	if($conn->query($sql_db)){
		echo "\r\nDatabase has been created successfully.";
		$conn->next_result();
	}else{
		echo "\r\nError creating database " . $conn->error;
		die("Stopping process");
	}



	//creating database tables
	$sql_table = "USE carsome;"; //selecting database
	//dealer
	$sql_table .= "CREATE TABLE IF NOT EXISTS dealer(
			id int AUTO_INCREMENT NOT NULL, name varchar(20) NOT NULL,
			PRIMARY KEY(id)
			);";
	//bid
	$sql_table .= "CREATE TABLE IF NOT EXISTS bid(
			id int AUTO_INCREMENT NOT NULL, dealer_id int NOT NULL, proposed_bid int NOT NULL,
			bid_status varchar(20) NOT NULL, creation_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY(id), FOREIGN KEY(dealer_id) REFERENCES dealer(id)
			);";
	//execute query creating table
        if(!$conn->multi_query($sql_table))
                echo "\r\nError creating data tables: " . $conn->error;
        do{
                if($result = $conn->store_result())
                        $result->free();
                if(!$conn->next_result()){
                        echo "\r\nData table has been created successfully.";
                }
        }while($conn->next_result());


	
	//creating dealers
	$sql_dealer = "USE carsome;";
	$counter = 0;
	do{
		$counter++;
		$dealer_name = "dealer" . $counter;
		$sql_dealer .= "INSERT INTO dealer(name) VALUES('$dealer_name');";

	}while($counter < 100);
	//executing query create dealer
	if(!$conn->multi_query($sql_dealer))
		echo "\r\nError creating dealers: " . $conn->error;
	do{
                if($result = $conn->store_result())
                        $result->free();
                if(!$conn->next_result()){
                        echo "\r\nDealers has been created successfully.\r\n";
                }
        }while($conn->next_result());
	
	
	//close connection
	$conn->close();

?>
